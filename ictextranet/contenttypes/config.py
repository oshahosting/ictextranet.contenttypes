PROJECTNAME = 'ictextranet.contenttypes'

BLOG_NAME = "Blog"
GALLERY_NAME = "Gallery"

ADD_PERMISSIONS = {
    'InternalLink': 'ictextranet.contenttypes: Add Internal Link',
}
