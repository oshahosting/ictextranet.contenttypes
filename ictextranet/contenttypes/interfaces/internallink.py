from zope import schema
from zope.interface import Interface

from ictextranet.contenttypes import ictextranetMessageFactory as _


class IInternalLink(Interface):
    """
    An Internal Link is a reference to another object in the database
    """
