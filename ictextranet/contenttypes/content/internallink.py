"""Definition of the Internal Link content type
"""
import urlparse
from urllib import quote

from AccessControl import ClassSecurityInfo
from zope.formlib import form
from zope.interface import implements
from zope.app.pagetemplate import viewpagetemplatefile

from Products.Archetypes import atapi
from Products.ATContentTypes.content import schemata
from Products.ATContentTypes.content import link
from Products.ATContentTypes.content.base import ATCTContent
from Products.ATContentTypes.content.schemata import ATContentTypeSchema
from Products.ATReferenceBrowserWidget.ATReferenceBrowserWidget import\
     ReferenceBrowserWidget
from Products.CMFCore.permissions import ModifyPortalContent
from Products.CMFCore.permissions import View
from plone.app.form import base

from ictextranet.contenttypes import ictextranetMessageFactory as _
from ictextranet.contenttypes.interfaces import IInternalLink
from ictextranet.contenttypes.config import PROJECTNAME


InternalLinkSchema = schemata.ATContentTypeSchema.copy() + atapi.Schema((
    atapi.ReferenceField(
        'target',
        multiValued=0,
        relationship="internal_link_target",
        widget=ReferenceBrowserWidget(
            default_search_index='SearchableText',
            description='Select the alias target'
            )
        ),
))

class InternalLink(ATCTContent):
    """
    The code is mostly copied from ATLink, the only difference is that
    it uses a ReferenceBrowserWidget and the field is called 'target'
    instead of remote url.
    """

    implements(IInternalLink)
    schema = InternalLinkSchema
    schema["title"].required = False
    schema["title"].widget.visible["edit"] = "invisible"

    archetype_name = 'InternalLink'
    _atct_newTypeFor = {
        'portal_type' : 'Internal Link',
        'meta_type' : 'Internal Link'
        }

    cmf_edit_kws   = ('target_path', )
    security       = ClassSecurityInfo()

    def Title(self):
        if self.title != "":
            title = self.title
        else:
            target = self.reference_catalog.lookupObject(self["target"])
            title = ""
            if target:
                title = target.Title()
                self.title = u"Alias for %s" %title
        return title

    security.declareProtected(View, 'target_path')
    def target_path(self):
        """CMF compatibility method
        """
        return self.getTarget()

    security.declarePrivate('cmf_edit')
    def cmf_edit(self, target_path=None, **kwargs):
        if not target_path:
            target_path = kwargs.get('target_path', None)
        self.update(target = target_path, **kwargs)

    security.declareProtected(View, 'getTarget')
    def getTarget(self):
        """Sanitize output
        """
        value = self.Schema()['target'].get(self)
        if not value: value = '' # ensure we have a string
        return quote(value, safe='?$#@/:=+;$,&%')

atapi.registerType(InternalLink, PROJECTNAME)
