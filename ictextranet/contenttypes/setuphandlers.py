import logging
from Products.CMFCore.utils import getToolByName

from config import BLOG_NAME
from config import GALLERY_NAME

log = logging.getLogger('ictextranet.contenttypes/setuphandlers.py')

def setupBlogging(context):
    """ Create a new Blog type by copying the portal_factory info for normal
        folders and modifying it a bit.
        Add the Blog type to portal_factory.
        Restrict the Blog Entry type so that it can only be created inside
        a Blog.
    """
    if context.readDataFile("ictextranet-contenttypes.txt") is None:
        return

    # copy folder for our blog
    site = context.getSite()
    portal_types = getToolByName(site, 'portal_types')
    if BLOG_NAME not in portal_types.objectIds():
        cb_copy_data = portal_types.manage_copyObjects(['Folder'])
        paste_data = portal_types.manage_pasteObjects(cb_copy_data)
        temp_id = paste_data[0]['new_id']
        portal_types.manage_renameObject(temp_id, BLOG_NAME)
        getattr(portal_types, BLOG_NAME).title = BLOG_NAME
        log.info("Duplicated 'Folder' FTI info as '%s'" % BLOG_NAME)

    # tweak Blog FTI settings
    blog = getattr(portal_types, BLOG_NAME)
    blog._updateProperty(
                    'description',
                    u'A blog folder that can contain blog entries.'
                    )

    blog._updateProperty('global_allow', False)
    blog._updateProperty('filter_content_types', True)
    blog._updateProperty('allowed_content_types', ('Blog Entry',))
    log.info("Tweaked %s FTU settings" % BLOG_NAME)

    # let Blog use the portal factory
    factory = getToolByName(site, 'portal_factory')
    types = factory.getFactoryTypes().keys()
    if BLOG_NAME not in types:
        types.append(BLOG_NAME)
        factory.manage_setPortalFactoryTypes(listOfTypeIds=types)
        log.info("Added Blog to portal factory")

    # Don't allow blog entries to be created outside of Blogs
    if "Blog Entry" not in portal_types.objectIds():
        qi = getToolByName(site, 'portal_quickinstaller')
        qi.installProduct("Scrawl")
    blog_entry = portal_types.getTypeInfo('Blog Entry')
    blog_entry._updateProperty('global_allow', False)


def setupGallery(context):
    """ Create a new Gallery type by copying the portal_factory info
        for normal folders and modifying it a bit.

        Add the Gallery type to portal_factory.
        Adjust the default Gallery Settings
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return

    # Copy the Folder type to use it for Gallery
    site = context.getSite()
    portal_types = getToolByName(site, 'portal_types')
    if GALLERY_NAME not in portal_types.objectIds():
        cb_copy_data = portal_types.manage_copyObjects(['Folder'])
        paste_data = portal_types.manage_pasteObjects(cb_copy_data)
        temp_id = paste_data[0]['new_id']
        portal_types.manage_renameObject(temp_id, GALLERY_NAME)
        getattr(portal_types, GALLERY_NAME).title = GALLERY_NAME
        log.info("Duplicated 'Folder' FTI info as '%s'" % GALLERY_NAME)

    # tweak Gallery FTI settings
    gallery = getattr(portal_types, GALLERY_NAME)
    gallery._updateProperty(
                    'description',
                    u'A gallery folder that can contain gallery entries.'
                    )

    gallery._updateProperty('global_allow', True)
    gallery._updateProperty('filter_content_types', True)
    gallery._updateProperty('allowed_content_types', ('Image',))
    gallery._updateProperty("immediate_view", "galleryview")
    gallery._updateProperty("default_view", "galleryview")
    gallery._updateProperty("view_methods", "galleryview")
    log.info("Tweaked %s FTU settings" % GALLERY_NAME)

    # let Gallery use the portal factory
    factory = getToolByName(site, 'portal_factory')
    types = factory.getFactoryTypes().keys()
    if GALLERY_NAME not in types:
        types.append(GALLERY_NAME)
        factory.manage_setPortalFactoryTypes(listOfTypeIds=types)
        log.info("Added Gallery to portal factory")
